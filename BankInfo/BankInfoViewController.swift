//
//  BankInfoViewController.swift
//  BankInfo
//
//  Created by Dmitry Labetsky on 03/11/16.
//  Copyright © 2016 DmiLab. All rights reserved.
//

import UIKit
import CoreLocation

class BankInfoViewController: UIViewController,
                              UITableViewDataSource,
                              UITableViewDelegate,
                              CLLocationManagerDelegate {

    private let content = BankInfoContent()

    let cellReuseIdentifier = "BankInfoCell"

    let locationManager = CLLocationManager()

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!

    // MARK: - View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        content.onItemsUpdate = { [weak self] in
            self?.tableView.reloadData()
            self?.spinner.stopAnimating()
        }

        loadInfo()

        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }

    // MARK: - UITableViewDataSource

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content.items.count
    }

    // MARK: - UITableViewDelegate

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)

        if let bankInfoCell = cell as? BankInfoCell {
            bankInfoCell.bankInfo = content.items[indexPath.row]
            bankInfoCell.userLocation = locationManager.location
        }

        return cell
    }

    // MARK: - Loading data

    private func loadInfo() {
        spinner.startAnimating()
        content.loadInfo()
    }

    // MARK: - Location

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            startTrackingLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        tableView.reloadData()
    }

    private func startTrackingLocation() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }

}
