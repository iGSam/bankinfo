//
//  BankInfoCell.swift
//  BankInfo
//
//  Created by Dmitry Labetsky on 03/11/16.
//  Copyright © 2016 DmiLab. All rights reserved.
//

import UIKit
import CoreLocation

private let distanceLabelPostFix = "km"

class BankInfoCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var scheduleLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!

    var bankInfo: BankInfo? {
        didSet {
            if bankInfo !== oldValue {
                updateBankInfo()
            }
        }
    }
    var userLocation: CLLocation? {
        didSet {
            if userLocation != oldValue {
                updateLocation()
            }
        }
    }

    // MARK: - Update UI

    private func updateBankInfo() {
        titleLabel.text = bankInfo?.title
        addressLabel.text = bankInfo?.address
        scheduleLabel.text = bankInfo?.schedule
        updateLocation()
    }

    private func updateLocation() {
        guard let bankLocation = bankInfo?.location?.clLocation,
              let userLocation = userLocation else {
            distanceLabel.text = ""
            return
        }

        let metersInKm = 1000.0
        let distance = userLocation.distance(from: bankLocation) / metersInKm
        distanceLabel.text = "\(String(format: "%.1f", distance))\(distanceLabelPostFix)"
    }

}
