//
//  BankInfo.swift
//  BankInfo
//
//  Created by Dmitry Labetsky on 03/11/16.
//  Copyright © 2016 DmiLab. All rights reserved.
//

import Foundation
import ObjectMapper

class BankInfo: Mappable {

    var title: String?
    var address: String?
    var schedule: String?
    var location: Location?

    required init?(map: Map) {}

    func mapping(map: Map) {
        title <- map["title"]
        address <- map["address"]
        schedule <- map["schedule"]
        location <- map["location"]
    }
    
}
