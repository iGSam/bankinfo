//
//  BankInfoContent.swift
//  BankInfo
//
//  Created by Dmitry Labetsky on 03/11/16.
//  Copyright © 2016 DmiLab. All rights reserved.
//

import Foundation
import CoreLocation

class BankInfoContent {

    private(set) var items: [BankInfo] = []

    private let bankInfoNetworkService = BankInfoNetworkService()

    var onItemsUpdate: (() -> ())?

    func loadInfo() {
        bankInfoNetworkService.requestBankInfoListing { [weak self] bankInfoItems in
            self?.items = bankInfoItems ?? []
            self?.onItemsUpdate?()
        }
    }

}
