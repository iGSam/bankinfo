//
//  BankInfoListing.swift
//  BankInfo
//
//  Created by Dmitry Labetsky on 03/11/16.
//  Copyright © 2016 DmiLab. All rights reserved.
//

import Foundation
import ObjectMapper

class BankInfoListing: Mappable {

    var items: [BankInfo]?

    required init?(map: Map) {}

    func mapping(map: Map) {
        items <- map["items"]
    }
    
}
