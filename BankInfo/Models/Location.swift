//
//  Location.swift
//  BankInfo
//
//  Created by Dmitry Labetsky on 03/11/16.
//  Copyright © 2016 DmiLab. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreLocation

class Location: Mappable {

    var latitude: Double?
    var longitude: Double?

    required init?(map: Map) {}

    func mapping(map: Map) {
        latitude <- map["lat"]
        longitude <- map["lng"]
    }

}

extension Location {

    var clLocation: CLLocation? {
        guard let latitude = latitude, let longitude = longitude else {
            return nil
        }
        return CLLocation(latitude: latitude, longitude: longitude)
    }

}
