//
//  BankInfoNetworkService.swift
//  BankInfo
//
//  Created by Dmitry Labetsky on 03/11/16.
//  Copyright © 2016 DmiLab. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

private let basicApiUrl = "http://gymn652.ru/tmp/unicorn.txt-2.json"

class BankInfoNetworkService {

    func requestBankInfoListing(completion: @escaping ([BankInfo]?) -> ()) {
        Alamofire.request(basicApiUrl).responseObject { (response: DataResponse<BankInfoListing>) in
            completion(response.result.value?.items)
        }
    }

}
