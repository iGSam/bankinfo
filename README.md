# BankInfo #

##Тестовое задание:
* Загрузить данные о ближайших отделениях банка по ссылке: http://gymn652.ru/tmp/unicorn.txt-2.json
* Отобразить в таблице название организации, адрес, режим работы и расстояние.

##Installation
Run pod install before building the project

##Requirements
* iOS 9.0+
* Xcode 8.0+
* Swift 3.0+